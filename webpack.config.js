const path = require('path');

module.exports = {
  entry: './app/app.js',
  output: {
    path: path.resolve(__dirname, './app/dist'),
    filename: 'app.bundle.js',
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'app'),  // URL base 를 어느 디렉토리를 할지 지정.
    // webpack-dev-server 로 띄우면 app.bundle.js가 메모리 안에 만들어져 재공되고 / 디렉토리 밑에 있는 것처럼 처리된다.
    // 그래서 index.html 에 ./dist/app.bundle.js와 같이 제공하면 찾지를 못한다. 그래서 메모리상의 app.bundle.js에 ./dist/app.bundle.js와 같이 만들어 주기 위해
    // 아래와 같이 publicPath를 지정해줄 수 있다.
    // 이렇게 하면 브라우저가 ./dist/app.bundle.js를 요청할 때, 메모리상에서 app.bundle.js를 /dist/ 밑에 만들어 두었기 때문에 정상적으로 서비스가 된다.
    publicPath: '/dist/',   // 메모리상의 app.bundle.js가 위치할 상대적 경로를 만들어 준다.
    watchContentBase: true,  // contentBase 이하에 있는 파일들 중에서 webpack에 의해 bundling 되지 않는 파일도 수정하면 바로 반영되도록 해줌.
  },
  /*
  Project is running at http://0.0.0.0:8080/
  webpack output is served from /dist/
  Content not from webpack is served from /home/node/MyDocker/pluralsight/optimizing-web-apps-webpack/app
  webpack-dev-server 실행하면 위와 같은 메시지가 나오는데, 두번째 줄이 publicPath에 의해 생성된 것이고,
  세번째 줄이 contentBase에 의해 지정된 것이다.
  */

}
